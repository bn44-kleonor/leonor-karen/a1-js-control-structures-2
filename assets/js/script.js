/*================================================================================

MODULE: WD004-S3-A2
  1. Display in the console all numbers FROM -10 to 20
  2. Display all odd numbers BETWEEN 10 and 40
  3. Create a variable `evenSum` and assign it an initial value of 0.
      Create a variable `oddSum` and assign it an initial value of 0.
      Iterate through all integers from 0 to 50.
      Compute for the sum of all odd integers.
      Compute for the sum of all even integers.
      Display the sums.

      EXPECTED OUTPUT: 
      The sum of all even numbers from 0 to 50 is 650;
      The sum of all odd numbers from 0 to 50 is 625;


  Push as: a1-js-control-structures-2
==================================================================================*/

//Act no. 1
  console.log("1. Display in the console all numbers FROM -10 to 20")
  let num = -10;
  while (num <= 20) {
    //if (num >= 10) {
      console.log(num);
    //}
    num++;
  }


//Act no. 2
  console.log("2. Display all odd numbers BETWEEN 10 and 40")
  num = 1;
  while(num <= 40) {
    if (num >=10) {
      if (num%2 !== 0) {
        console.log(num);
      }
    }
    num++;
  }


//Act no. 3
/*   3. Create a variable `evenSum` and assign it an initial value of 0.
      Create a variable `oddSum` and assign it an initial value of 0.
      Iterate through all integers from 0 to 50.
      Compute for the sum of all odd integers.
      Compute for the sum of all even integers.
      Display the sums.

      EXPECTED OUTPUT: 
      The sum of all even numbers from 0 to 50 is 650;
      The sum of all odd numbers from 0 to 50 is 625;     */

let evenSum = 0;
let oddSum = 0;

for(let i=0; i<=50; i++) {
  if (i%2 !== 0) {
    oddSum += i;
  } else {
    evenSum += i;
  }
}

console.log(`The sum of all even numbers from 0 to 50 is ${evenSum}`);
console.log(`The sum of all even numbers from 0 to 50 is ${oddSum}`);


